import os
import time
class Reconstructor:

    def __init__(self):
        try:
            file = open("words.txt")
            lines = file.readlines()
            words = []
            self.largest = 0
            for x in lines:
                words.append(x.strip())
                if len(x) > self.largest:
                    self.largest = len(x)
            print(self.largest)
            self.dictionary = {x: x for x in words} 

        except TypeError as e:
            print("FILE NOT FOUND")
            print(e)

    def checkFor(self, line):
        notfound = "NO WORD FOUND"
        word = ''
        for x in line:
            word = word + x
            if len(word) > self.largest: break
            #print(word in self.dictionary)
            if word in self.dictionary and word == line:
                return(word)

            elif word in self.dictionary:
                #print(line[len(word):])
                #print(word)
                #time.sleep(3)
                x =  self.checkFor(line[len(word):])
                if x != notfound:
                    return(word + " " + x)
        
        return(notfound)
                    


        #while word is not 'zzz':
         #   word = input("Give word: ")
          #  print("Word is in dictionary: ", word in self.dictionary)



x = Reconstructor()
txt_files = [x for x in os.listdir('.') if x.endswith('.txt')]
print(txt_files)
name = input("File Name: ")
try:
    file = open(name)
    lines = file.readline().strip()
    st = time.time()
    print(x.checkFor(lines))
    print(time.time() - st)
except TypeError as e:
    print(e) 



